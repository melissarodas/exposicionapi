@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Ingenieria del Softwarev 4') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Implementando un API - Laravel 8') }}
                </div>
            </div>
        </div>
        @foreach ( $usuariosArray as  $user)
        <div class="col-md-6">
            <ul class="list-group mt-2 mb-4">
                <li class="list-group-item active">Nombre: {{$user['name']}}</li>
                <li class="list-group-item ">Username: {{$user['username']}}</li>
                <li class="list-group-item ">Email: {{$user['email']}}</li>
                <li class="list-group-item ">Direccion: {{$user['address']['street']}}</li>
                <li class="list-group-item ">Telefono: {{$user['phone']}}</li>
                <li class="list-group-item ">Sitio web: {{$user['website']}}</li>
            </ul>
        </div>

    @endforeach
    </div>
</div>
@endsection
